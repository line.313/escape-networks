/*
 * Copyright (c) 2020, IPD Koziolek. All rights reserved.
 */

package edu.kit.informatik.model.resources;

import edu.kit.informatik.presenter.manager.commands.PrintNetwork;

/**
 * This class provides resources.
 * In big projects ResourceBundles would be used instead.
 * 
 * @author Lucas Alber
 * @version 1.0
 */

public final class Errors {
    /** Error-Message if a parsing operation fails. Format with actual and expected type. */
    public static final String PARSING_FAILED = "'%s' cannot be parsed as %s.";
    /** Error-Message if parameter parsing fails. Format with parameter index and detailed message */
    public static final String PARAMETER_PARSING = "failed to parse parameter on index %d: %s";
    /** Error-Message if parameter parsing fails. Format with parameter description, parameter index
     * and detailed message */
    public static final String PARAMETER_PARSING_DESC = "failed to parse parameter (%s) on index %d: %s";
    /** Error-Message if put-Operation of a bundle fails */
    public static final String PARAM_CLASS_MATCH 
            = "class %s does not match %s. Parameter could not be put into the bundle.";
    /** Error-Message if command regex does not match. Format with supplied command */
    public static final String COMMAND_REGEX_NOT_MATCHED 
            = "the supplied command '%s' does not match the command pattern.";
    /** Error-Message if a unknown command name is detected. Format with command name */
    public static final String COMMAND_NOT_KNOWN = "the command '%s' is not known.";
    /** Error-Message if parameter count does not match. */
    public static final String PARAMETER_COUNT = "parameter count does not match.";
    /** Error-Message if parameter count does not match. Format with min and max expected count and actual count */
    public static final String PARAMETER_COUNT_BETWEEN = PARAMETER_COUNT
            + " Expected between %d and %d parameter but got %d.";
    /** Error-Message if parameter count does not match. Format with expected count and actual count */
    public static final String PARAMETER_COUNT_EXACTLY = PARAMETER_COUNT
            + " Expected exactly %d parameter but got %d.";
    /** Error-Message if something was not implemented, but should */
    public static final String NOT_IMPLEMENTED = "%s not implemented!";
    /** Error-Message if a command did not succeed but no message was supplied */
    public static final String COMMAND_ENDED_ERROR = "command did not succeed.";
    /** Error-Message that the parameter did not match a certain regex. Format with parameter and regex */
    public static final String PARAMETER_REGEX_DOES_NOT_MATCH = "the parameter '%s' does not match '%s'.";
    /** Error-Message if network name is invalid */
    public static final String NETWORK_NAME = "network identifier is invalid.";
    /** Error-Message if network name is to long */
    public static final String NETWORK_NAME_LENGTH = NETWORK_NAME
            + "Expected less or equal %d charakters but got %d.";
    /** Error-Message if network name is to long */
    public static final String NETWORK_NAME_CAPITAL = NETWORK_NAME
            + "Expected only capital letters (%s) but got %s.";
    /** Error-Message if no network with given identifier is in the database */
    public static final String NETWORK_NOT_KNOWN = "the network %s is not known.";
    /** Error-Message if tried to add multiple edges to existing network */
    public static final String ADD_MULTIPLE_EDGES = "there is en existing network \"%s\" (try \"" + PrintNetwork.REGEX
            + " %s\"). Either use another identifier or add edges separately.";

    /* private utility-class constructor */
    private Errors() { }
}
