package edu.kit.informatik.model.resources;

/**
 * This class provides resources.
 *
 * Inherited by proposed solutions
 * 
 * @author Linus Ostermayer
 * @version 1.0
 */
public final class Messages {

    /** The message if a operation succeeded */
    public static final String SUCCESS = "OK";
    /** The message if a list is empty */
    public static final String EMPTY = "EMPTY";
    /** The message if a new escape network has been added */
    public static final String NETWORK_ADDED = "Added new escape network with identifier %s";

    /* private utility-class constructor */
    private Messages() { }
}
