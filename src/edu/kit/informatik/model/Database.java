package edu.kit.informatik.model;

import edu.kit.informatik.model.network.EscapeNetwork;
import edu.kit.informatik.model.network.Graph;
import edu.kit.informatik.util.EscapeNetworkComparator;

import java.util.Set;
import java.util.TreeSet;

/**
 * This class describes an command provider for an escape route network manager.
 *
 * @author Linus Ostermayer
 * @version %I%, %G%
 */
public final class Database {

    private final Set<EscapeNetwork> networkSet;

    public Database() {
        networkSet = new TreeSet<>(new EscapeNetworkComparator());
    }

    public EscapeNetwork addNetwork(String name) {
        EscapeNetwork network = new EscapeNetwork(name);
        networkSet.add(network);
        return network;
    }

    public Set<EscapeNetwork> getNetworkSet() {
        return networkSet;
    }

    public boolean contains(String name) {
        return !(getGraph(name) == null);
    }

    public boolean nameAvailable(String name) {
        return getGraph(name) == null;
    }

    public EscapeNetwork getNetwork(String name) {
        for (EscapeNetwork network : networkSet) {
            if (network.getIdentifier().equals(name)) {
                return network;
            }
        }
        return null;
    }

    public Graph getGraph(String name) {
        EscapeNetwork network = getNetwork(name);
        return network == null ? null : network.getGraph();
    }
}
