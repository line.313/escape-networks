package edu.kit.informatik.model.network;

import edu.kit.informatik.model.resources.Errors;
import edu.kit.informatik.util.Pair;

import java.util.HashMap;
import java.util.Map;

public class EscapeNetwork {

    public static final String IDENTIFIER_REGEX = "[A-Z]{1,6}";

    private final String identifier;
    private final Graph graph;
    private final Map<Pair<Node, Node>, Integer> flowMap = new HashMap<>();

    /**
     * Constructs a new escape Network.
     *
     * needs to be checked before:</br>
     * - correct identifier expression</br>
     * - unique in given database
     *
     * @param name the identifier of the created escape network
     */
    public EscapeNetwork(String name) {
        if (!name.matches(IDENTIFIER_REGEX)) {
            throw new IllegalArgumentException(Errors.NETWORK_NAME);
        }
        this.identifier = name;
        this.graph = new Graph();
    }

    public String getIdentifier() {
        return identifier;
    }

    public Graph getGraph() {
        return graph;
    }

    public int getGraphSeize() {
        return graph.getSeize();
    }

    public Map<Pair<Node, Node>, Integer> getFlowMap() {
        return flowMap;
    }

    public void addEdge(String start, int capacity, String target) {

    }

    @Override
    public String toString() {
        return this.identifier + " " + getGraphSeize();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof EscapeNetwork)) {
            return false;
        }
        EscapeNetwork network = (EscapeNetwork) obj;

        return this.identifier == network.identifier;
    }

    @Override
    public int hashCode() {
        return this.identifier.hashCode();
    }
}
