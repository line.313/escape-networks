package edu.kit.informatik.model.network;

public class Edge {

    private final Node target;
    private int capacity;
    private int flow;

    public Edge(Node target, int capacity) {
        this.target = target;
        this.capacity = capacity;
    }

    public Node getTarget() {
        return this.target;
    }

    public int getCapacity() {
        return capacity;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Edge)) {
            return false;
        }
        Edge edgeObj = (Edge) obj;

        return this.target.equals(edgeObj.target);
    }

    @Override
    public int hashCode() {
        //as String.hashCode() is calculated by 2 potency's, the hashCode is different from the target node ones
        return this.target.hashCode() + 1;
    }

}
