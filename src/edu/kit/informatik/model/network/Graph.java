package edu.kit.informatik.model.network;

import edu.kit.informatik.util.NodeComparator;

import java.util.*;

/**
 * This class describes an command provider for an escape route network manager.
 *
 * @author Linus Ostermayer
 * @version %I%, %G%
 */
public class Graph {

    private final Set<Node> nodes;

    /**
     * Creates a new graph instance
     */
    public Graph() {
        this.nodes = new TreeSet<>(new NodeComparator());
    }

    public Set<Node> getNodes() {
        return nodes;
    }

    public Node getNode(String id) {
        for (Node node : this.nodes) {
            if (node.getId() == id) {
                return node;
            }
        }
        return null;
    }

    public int getSeize() {
        return nodes.size();
    }

    public void addEdge(String start, int capacity, String target) {
        final Node node = getNode(start);

    }

    @Override
    public String toString() {
        if (nodes.isEmpty()) {
            return null;
        }
        StringJoiner joiner = new StringJoiner(System.lineSeparator());
        for (Node node : nodes) {
            joiner.add(node.toString());
        }
        return joiner.toString();
    }
}
