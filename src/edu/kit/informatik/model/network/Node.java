package edu.kit.informatik.model.network;

import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

public class Node {

    private final String id;
    private List<Edge> edges;

    public Node(String name) {
        this.id = name;
        this.edges = new ArrayList<Edge>();
    }

    public String getId() {
        return this.id;
    }

    public List<Edge> getEdges() {
        return edges;
    }

    public void addEdge(int capacity, String target) {

    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Node)) {
            return false;
        }
        Node nodeObj = (Node) obj;

        return this.id == nodeObj.id;
    }

    @Override
    public int hashCode() {
        return this.id.hashCode();
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for (Edge edge: this.edges) {
            builder.append(this.id);
            builder.append(edge.getCapacity());
            builder.append(edge.getTarget().id);
            builder.append(System.lineSeparator());
        }
        return builder.toString();
    }
}
