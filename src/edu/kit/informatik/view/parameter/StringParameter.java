/*
 * Copyright (c) 2021, KASTEL. All rights reserved.
 */

package edu.kit.informatik.view.parameter;

import edu.kit.informatik.model.resources.Errors;

/**
 * This class describes a string parameter.
 * 
 * @author Lucas Alber
 * @version 1.1
 */
public class StringParameter extends Parameter<String> {

    private static final String REGEX_ALL = ".*";
    private final String regex;

    /**
     * Constructs a new string parameter accepting every string.
     */
    public StringParameter() {
        this(REGEX_ALL);
    }

    /**
     * Constructs a new string parameter which does only accept strings 
     * matching the supplied regex.
     *
     * @param regex the regular expression to match.
     */
    public StringParameter(final String regex) {
        super(String.class);
        this.regex = regex;
    }

    @Override
    public String fromString(final String str) throws ParseException {
        if (!str.matches(this.regex)) {
            throw new ParseException(String.format(Errors.PARAMETER_REGEX_DOES_NOT_MATCH, str, this.regex));
        }
        return str;
    }
}