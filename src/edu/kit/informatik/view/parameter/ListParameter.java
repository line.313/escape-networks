/*
 * Copyright (c) 2021, KASTEL. All rights reserved.
 */

package edu.kit.informatik.view.parameter;

import java.util.List;

import edu.kit.informatik.util.Utilities;
import edu.kit.informatik.model.resources.Errors;

/**
 * This class describes a list parameter. The parameter parses the to the 
 * object contained in a list which toString()-Representation matches the parameter.
 * If there are multiple objects in the list the object with the smallest index
 * is used.
 *
 * @author Lucas Alber
 * @version 1.0
 * @param <T> the type of objects in the list
 */
public class ListParameter<T> extends Parameter<T> {

    private static final String REGEX_OR = "|";
    private final List<T> list;

    /**
     * Constructs a new list parameter.
     *
     * @param list the list which contains the objects this parameter parses to.
     *      The parameter stores a reference to the list, that means changes to the list
     *      are directly reflected to this parameter.
     * @param type the type of object this parameter parses to
     */
    public ListParameter(final List<T> list, final Class<T> type) {
        super(type);
        this.list = list;
    }

    @Override
    public T fromString(final String str) throws ParseException {
        for (T object : this.list) {
            if (object != null && object.toString().equals(str)) {
                return object;
            }
        }
        throw new ParseException(String.format(Errors.PARAMETER_REGEX_DOES_NOT_MATCH, 
            str, 
            Utilities.join(REGEX_OR, this.list)));
    }

}