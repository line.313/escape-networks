package edu.kit.informatik.view.parameter;

import edu.kit.informatik.model.resources.Errors;

/**
 * This class describes a parameter for network/vertices identifiers.
 *
 * Such parameter contains a maximum of 6 capital letters.
 *
 * @author Linus.Ostermayer
 * @version %I%, %G%
 */
public class IdentifierParameter extends StringParameter {

    /** The maximum length of an identifier parameter */
    public static final int NET_MAX_LENGTH = 6;
    /** The allowed characters */
    public static final String NET_REGEX = "[A-Z]";

    /**
     * Constructs a new parameter.
     */
    public IdentifierParameter() {
        super();
    }

    @Override
    public String fromString(final String str) throws ParseException {
        if (str.length() > NET_MAX_LENGTH) {
            throw new ParseException(String.format(Errors.NETWORK_NAME_LENGTH, NET_MAX_LENGTH, str.length()));
        }
        if (!str.matches(NET_REGEX)) {
            throw new ParseException(String.format(Errors.NETWORK_NAME_CAPITAL, NET_REGEX, str));
        }
        return str;
    }

}
