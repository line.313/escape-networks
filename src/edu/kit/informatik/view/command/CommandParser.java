/*
 * Copyright (c) 2021, KASTEL. All rights reserved.
 */

package edu.kit.informatik.view.command;

import edu.kit.informatik.util.Pair;

import java.util.List;

/**
 * This interface describes a parser for commands.
 * The parser parses a string into a command name and a List of parameters.
 * 
 * @author Lucas Alber
 * @version 1.0
 */
public interface CommandParser {

    /**
     * Parses the given string into a {@link Pair} of command name and a List of parameters.
     *
     * @param input the input string
     *
     * @return a {@link Pair} of command name and a List of parameters as string representation.
     *      Returns {@code null} if the string could not be parsed.
     */
    Pair<String, List<String>> parse(String input);
}