package edu.kit.informatik;

import edu.kit.informatik.model.Database;
import edu.kit.informatik.presenter.Quit;
import edu.kit.informatik.presenter.manager.ManagementCommands;
import edu.kit.informatik.util.Input;
import edu.kit.informatik.util.Output;
import edu.kit.informatik.view.Session;
import edu.kit.informatik.view.command.IPDCommandParser;

/**
 * This class describes an command provider for an escape route network manager.
 *
 * @author Linus Ostermayer
 * @version %I%, %G%
 */
public final class Application {

    // inherited by proposed solutions
    // Abstraction from Terminal class,
    // With method references the anonymous inner classes can be replaced by Terminal::printLine.
    // This is not part of the programming lecture
    private static final Output OUTPUT = new Output() {
        @Override
        public void output(final String string) {
            Terminal.printLine(string);
        }
    };
    private static final Output ERROR_OUTPUT = new Output() {
        @Override
        public void output(final String string) {
            Terminal.printError(string);
        }
    };
    private static final Input INPUT = new Input() {
        @Override
        public String read() {
            return Terminal.readLine();
        }
    };

    // utility-class constructor
    private Application() { }


    /**
     * The main entry point to the application.
     *
     * @param args the command-line arguments are ignored
     */
    public static void main(final String[] args) {

        final Database data = new Database();
        final Session session = new Session(OUTPUT, ERROR_OUTPUT,
            INPUT, new IPDCommandParser());

        session.addCommandSupplier(new ManagementCommands(data));
        session.addCommandSupplier(new Quit(session));
        session.interactive();
    }

}
