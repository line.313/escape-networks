package edu.kit.informatik.presenter.manager.commands;

import edu.kit.informatik.model.Database;
import edu.kit.informatik.model.network.EscapeNetwork;
import edu.kit.informatik.view.command.Command;
import edu.kit.informatik.view.parameter.IdentifierParameter;
import edu.kit.informatik.view.parameter.Parameter;
import edu.kit.informatik.view.parameter.ParameterBundle;
import edu.kit.informatik.view.parameter.StringParameter;

import java.util.List;

/**
 * This class describes an command provider for an escape route network manager.
 *
 * @author Linus Ostermayer
 * @version %I%, %G%
 */
public class CalculateFlow extends Command {
    private static final String REGEX = "flow";

    private final Parameter<String> network = new StringParameter(EscapeNetwork.IDENTIFIER_REGEX);
    private final Database database;

    /**
     * Constructs a new instance.
     *
     * @param database the escape networks database
     */
    public CalculateFlow(Database database) {
        this.database = database;
    }

    @Override
    public List<Parameter<?>> getParameters() {
        return List.of(
            this.network
        );
    }

    @Override
    public String getRegex() {
        return REGEX;
    }


    @Override
    public Result apply(final ParameterBundle bundle) {

    }

}
