package edu.kit.informatik.presenter.manager.commands;

import edu.kit.informatik.model.Database;
import edu.kit.informatik.model.network.Edge;
import edu.kit.informatik.model.network.EscapeNetwork;
import edu.kit.informatik.model.network.Graph;
import edu.kit.informatik.model.network.Node;
import edu.kit.informatik.model.resources.Errors;
import edu.kit.informatik.view.command.Command;
import edu.kit.informatik.view.command.Result;
import edu.kit.informatik.view.parameter.IdentifierParameter;
import edu.kit.informatik.view.parameter.Parameter;
import edu.kit.informatik.view.parameter.ParameterBundle;
import edu.kit.informatik.view.parameter.StringParameter;

import java.util.List;
import java.util.Map;

/**
 * This class describes an command provider for an escape route network manager.
 *
 * @author Linus Ostermayer
 * @version %I%, %G%
 */
public class PrintNetwork extends Command {
    public static final String REGEX = "print";
    private static final String OUTPUT_SEPERATOR = System.lineSeparator();

    private final Parameter<String> network = new StringParameter(EscapeNetwork.IDENTIFIER_REGEX);
    private final Database database;

    /**
     * Constructs a new instance.
     *
     * @param database the escape networks database
     */
    public PrintNetwork(Database database) {
        this.database = database;
    }

    @Override
    public List<Parameter<?>> getParameters() {
        return List.of(
            this.network
        );
    }

    @Override
    public String getRegex() {
        return REGEX;
    }


    @Override
    public Result apply(final ParameterBundle bundle) {

        final String identifier = bundle.get(this.network);
        final Graph graph = this.database.getGraph(identifier);

        if (graph == null) {
            return new Result(Result.ResultType.FAILURE, String.format(Errors.NETWORK_NOT_KNOWN, identifier));
        }
        StringBuilder output = new StringBuilder();
        //as there is no option to remove vertices/edges any graph contains nodes
        for (Node node : graph.getNodes()) {
            for (Edge edge : node.getEdges()) {
                output.append(node.getId());
                output.append(edge.getCapacity());
                output.append(edge.getTarget().getId());
                output.append(System.lineSeparator());
            }
        }
        return new Result(Result.ResultType.SUCCESS, output.toString().trim());
    }

}
