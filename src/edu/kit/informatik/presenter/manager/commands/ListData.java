package edu.kit.informatik.presenter.manager.commands;

import edu.kit.informatik.model.Database;
import edu.kit.informatik.model.network.EscapeNetwork;
import edu.kit.informatik.model.network.Node;
import edu.kit.informatik.model.resources.Errors;
import edu.kit.informatik.model.resources.Messages;
import edu.kit.informatik.util.LexicographicComparator;
import edu.kit.informatik.util.Pair;
import edu.kit.informatik.util.Utilities;
import edu.kit.informatik.view.command.Command;
import edu.kit.informatik.view.command.Result;
import edu.kit.informatik.view.parameter.IdentifierParameter;
import edu.kit.informatik.view.parameter.Parameter;
import edu.kit.informatik.view.parameter.ParameterBundle;
import edu.kit.informatik.view.parameter.StringParameter;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * This class describes a
 *
 * @author Linus Ostermayer
 * @version %I%, %G%
 */
public class ListData extends Command {
    private static final String REGEX = "list";

    private final Parameter<String> network = new StringParameter(EscapeNetwork.IDENTIFIER_REGEX).setOptional(true);
    private final Database database;

    /**
     * Constructs a new instance.
     *
     * @param database the escape networks database
     */
    public ListData(Database database) {
        this.database = database;
    }

    @Override
    public List<Parameter<?>> getParameters() {
        return List.of(
            this.network
        );
    }

    @Override
    public String getRegex() {
        return REGEX;
    }


    @Override
    public Result apply(final ParameterBundle bundle) {
        String identifier = bundle.get(this.network);
        String output;

        if (identifier == null) {
            Set<EscapeNetwork> escapeNetworks = this.database.getNetworkSet();
            if (escapeNetworks.isEmpty()) {
                output = Messages.EMPTY;
            } else {
                output = Utilities.join(System.lineSeparator(), escapeNetworks);
            }
            return new Result(Result.ResultType.SUCCESS, output);
        } else {
            final EscapeNetwork network = database.getNetwork(identifier);

            if (network == null) {
                return new Result(Result.ResultType.FAILURE, String.format(Errors.NETWORK_NOT_KNOWN, identifier));
            }
//            List<Map.Entry<Pair<Node, Node>, Integer>> flowList = new ArrayList<>(network.getFlowMap().entrySet());
//            flowList.sort(Map.Entry.comparingByValue());
            List<String> list = new ArrayList<>();
            network.getFlowMap().forEach((pair,integer)->
                    list.add(integer + ' ' + pair.getFirst().getId() + ' ' + pair.getSecond().getId()));
            list.sort(new LexicographicComparator<>());
            return new Result(Result.ResultType.SUCCESS, Utilities.join(System.lineSeparator(), list));
        }

    }

}
