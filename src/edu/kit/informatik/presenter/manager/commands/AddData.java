package edu.kit.informatik.presenter.manager.commands;

import edu.kit.informatik.model.Database;
import edu.kit.informatik.model.network.EscapeNetwork;
import edu.kit.informatik.model.resources.Errors;
import edu.kit.informatik.view.command.Command;
import edu.kit.informatik.view.command.Result;
import edu.kit.informatik.view.parameter.Parameter;
import edu.kit.informatik.view.parameter.ParameterBundle;
import edu.kit.informatik.view.parameter.StringParameter;

import java.util.List;

public class AddData extends Command {
    private static final String REGEX = "add";
    private static final String EDGE_REGEX = "[a-z]{1,6}\\d+[a-z]{1,6}";
    private static final String EDGE_DELIMITER = ";";
    private static final String ARGUMENT_REGEX = "^" + EDGE_REGEX + "([" + EDGE_DELIMITER + "]" + EDGE_REGEX + ")*$";

    private final Parameter<String> network = new StringParameter(EscapeNetwork.IDENTIFIER_REGEX);
    private final Parameter<String> edges = new StringParameter(ARGUMENT_REGEX);
    private final Database database;

    /**
     * Constructs a new instance.
     *
     * @param database the escape networks database
     */
    public AddData(Database database) {
        this.database = database;
    }

    @Override
    public List<Parameter<?>> getParameters() {
        return List.of(
            this.network,
            this.edges
        );
    }

    @Override
    public String getRegex() {
        return REGEX;
    }


    @Override
    public Result apply(final ParameterBundle bundle) {
        final String name = bundle.get(this.network);
        EscapeNetwork network = database.getNetwork(name);
        final String[] edges = bundle.get(this.edges).split(EDGE_DELIMITER);

        if (network == null) {
            network = database.addNetwork(name);
            for (String edge : edges) {
                network
            }
        } else {
            if (edges.length != 1) {
                return new Result(Result.ResultType.FAILURE, String.format(Errors.ADD_MULTIPLE_EDGES, name, name));
            }

        }

    }

}
