package edu.kit.informatik.presenter.manager;

import java.util.List;

import edu.kit.informatik.view.command.Command;
import edu.kit.informatik.view.command.CommandSupplier;
import edu.kit.informatik.presenter.manager.commands.AddData;
import edu.kit.informatik.presenter.manager.commands.CalculateFlow;
import edu.kit.informatik.presenter.manager.commands.ListData;
import edu.kit.informatik.presenter.manager.commands.PrintNetwork;
import edu.kit.informatik.model.Database;

/**
 * This class describes an command provider for an escape route network manager.
 *
 * @author Linus Ostermayer
 * @version %I%, %G%
 */
public final class ManagementCommands implements CommandSupplier {

    private final List<Command> commands;

    /**
     * Constructs a command supplier for an escape route network manager.
     *
     * @param manager the escape route network manager to control
     */
    public ManagementCommands(final Database manager) {
        this.commands = List.of(
            new CalculateFlow(manager),
            new ListData(manager),
            new AddData(manager),
            new PrintNetwork(manager)
        );
    }

    @Override
    public List<Command> get() {
        // no copy needed since List.of() creates a immutable list and commands are immutable by contract.
        return this.commands;
    }
}
