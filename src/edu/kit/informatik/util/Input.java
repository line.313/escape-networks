/*
 * Copyright (c) 2021, KASTEL. All rights reserved.
 */

package edu.kit.informatik.util;

/**
 * This interface describes an input.
 * 
 * @author Lucas Alber
 * @version 1.0
 */
public interface Input {
    
    /**
     * Reads a string from the input. The method should block until the next
     * input is available.
     *
     * @return the next input string.
     */
    String read();
}