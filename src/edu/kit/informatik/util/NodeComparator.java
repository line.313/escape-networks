package edu.kit.informatik.util;

import edu.kit.informatik.model.network.Node;

import java.util.Comparator;

/**
 * This class describes a node comparator. Used for sorting
 * nodes  lexicographic by their id.
 *
 * @author Linus Ostermayer
 * @version %I%, %G%
 */
public class NodeComparator implements Comparator<Node> {

    @Override
    public int compare(final Node n1, final Node n2) {
        //follow Interface restrictions as identifier are unique (for a graph)
        return n1.getId().compareTo(n2.getId());
    }
}
