package edu.kit.informatik.util;

import edu.kit.informatik.model.network.EscapeNetwork;

import java.util.Comparator;

/**
 * This class describes a graph comparator. Used for sorting
 * graphs descending by their number of vertices and lexicographic ascending.
 *
 * @author Linus Ostermayer
 * @version 1.0
 */
public class EscapeNetworkComparator implements Comparator<EscapeNetwork> {

    @Override
    public int compare(final EscapeNetwork g1, final EscapeNetwork g2) {
        if (g1.getGraphSeize() != g2.getGraphSeize()) {
            return g2.getGraphSeize() - g1.getGraphSeize();
        }
        //follow Interface restrictions thus identifier are unique
        return g1.getIdentifier().compareTo(g2.getIdentifier());
    }
}
